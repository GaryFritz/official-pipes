IDENTITY_FILE=${IDENTITY_FILE:?"IDENTITY_FILE variable is missing."}
KNOWN_HOSTS_FILE=${KNOWN_HOSTS_FILE:?"KNOWN_HOSTS_FILE variable is missing."}
mkdir -p ~/.ssh
chmod -R go-rwx ~/.ssh/

cp ${KNOWN_HOSTS_FILE} ~/.ssh/known_hosts
cp ${IDENTITY_FILE} ~/.ssh/git_private_key
chmod 600 ~/.ssh/git_private_key
echo "IdentityFile ~/.ssh/git_private_key" >> ~/.ssh/config
git remote set-url origin ${BITBUCKET_GIT_SSH_ORIGIN}
git push origin master