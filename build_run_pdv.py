import glob
import json
import unittest

import pytest
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import time

from build import logger

RETRIES = 3
RETRIES_ON_CONDITION = 5
SLEEP_INTERVAL = 3


def retry():
    session = requests.Session()
    retry = Retry(
        total=RETRIES,
        read=RETRIES,
        connect=RETRIES,
        redirect=0,
        backoff_factor=0.5,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


@pytest.mark.usefixtures('filename')
class PDVTestCase(unittest.TestCase):

    def test_json_match_number_of_files(self):
        list_files = sorted(glob.glob('pipes/*.yml'))
        with open(self.test_file) as json_file:
            data = json.load(json_file)

        self.assertEqual(len(data), len(list_files))
        self.assertGreater(len(data), 0)

    def test_file_is_deployed(self):

        with open(self.test_file) as json_file:
            data = json.load(json_file)

        self.assertGreater(len(data), 0)

        with retry() as session:
            for i in range(RETRIES):
                try:
                    response = session.get(
                        f"https://bitbucket.org/bitbucketpipelines/official-pipes/raw/master/{self.test_file}",
                        headers={'Cache-Control': 'no-cache', 'Pragma': 'no-cache'}
                    )
                    response_data = response.json()
                    self.assertEqual(response_data, data)
                    self.assertGreater(len(response_data), 0)
                    break
                except AssertionError as error:
                    logger.error(f"{error} retrying {i} attempt")
                    time.sleep(SLEEP_INTERVAL)
            else:
                self.assertEqual(response_data, data)
