import re
import sys
import glob
import json
import logging
import subprocess

import warnings
from email import utils
from datetime import datetime
from typing import List, Optional
from dataclasses import dataclass, asdict
from concurrent.futures.thread import ThreadPoolExecutor

import yaml
import docker
import cerberus
import requests
from colorlog.colorlog import ColoredFormatter
from requests.exceptions import HTTPError

from constants import CREATED_AT_PIPES


def logger_config():
    """
        Setup the logging environment
    """

    clogger = logging.getLogger()
    format_str = '%(log_color)s%(levelname)s: %(message)s'
    date_format = '%Y-%m-%d %H:%M:%S'
    colors = {'DEBUG': 'green',
              'INFO': 'blue',
              'WARNING': 'bold_yellow',
              'ERROR': 'bold_red',
              'CRITICAL': 'bold_purple'}
    formatter = ColoredFormatter(format_str, date_format, log_colors=colors)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    clogger.addHandler(stream_handler)
    clogger.setLevel(logging.INFO)
    return clogger


logger = logger_config()

WHITELIST_PIPES = {
    'pipes/anchore-scan.yml': ['0.2.5 version available. Current version is 0.1.2'],
    'pipes/azure-aks-deploy.yml': ['pipes/azure-aks-deploy.yml yml field not equal'],
    'pipes/azure-aks-helm-deploy.yml': ['pipes/azure-aks-helm-deploy.yml yml field not equal'],
    'pipes/azure-arm-deploy.yml': ['pipes/azure-arm-deploy.yml yml field not equal'],
    'pipes/azure-cli-run.yml': ['pipes/azure-cli-run.yml yml field not equal'],
    'pipes/azure-functions-deploy.yml': ['pipes/azure-functions-deploy.yml yml field not equal'],
    'pipes/azure-vm-linux-script-deploy.yml': ['pipes/azure-vm-linux-script-deploy.yml yml field not equal'],
    'pipes/azure-vmss-linux-script-deploy.yml': ['pipes/azure-vmss-linux-script-deploy.yml yml field not equal'],
    'pipes/azure-web-apps-containers-deploy.yml': ['pipes/azure-web-apps-containers-deploy.yml yml field not equal'],
    'pipes/azure-web-apps-deploy.yml': ['1.0.4 version available. Current version is 1.0.3'],
    'pipes/cerberus-testing-runner.yml': ["pipes/cerberus-testing-runner.yml repository not valid: pipe.yml not valid: {'repository': ['required field']}", 'pipes/cerberus-testing-runner.yml yml definition in README.md is not valid'],
    'pipes/debricked-scan.yml': ['1.0.3 version available. Current version is 1.0.1'],
    'pipes/ld-find-code-refs-pipe.yml': ['1.5.1 version available. Current version is 1.0.0', 'pipes/ld-find-code-refs-pipe.yml yml field not equal'],
    'pipes/rollbar-notify.yml': ['pipes/rollbar-notify.yml yml field not equal'],
    'pipes/rolloutio-flag-templates.yml': ['pipes/rolloutio-flag-templates.yml yml field not equal'],
    'pipes/snyk-scan.yml': ['pipes/snyk-scan.yml yml field not equal'],
    'pipes/sonatype-nexus-repository-publish.yml': ['pipes/sonatype-nexus-repository-publish.yml yml field not equal'],
    'pipes/webhookrelay-functions-deploy.yml': ['0.2.5 version available. Current version is 0.2.4'],
    'pipes/whitesource-scan.yml': ['1.3.0 version available. Current version is 1.1.0', 'pipes/whitesource-scan.yml yml field not equal', "pipes/whitesource-scan.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"],
    'pipes/google-app-engine-deploy.yml': ["pipes/google-app-engine-deploy.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"],
    'pipes/google-gke-kubectl-run.yml': ["pipes/google-gke-kubectl-run.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"],
    'pipes/synopsys-detect.yml': ["pipes/synopsys-detect.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"],
    'pipes/meterian-scan.yml': ['1.0.6 version available. Current version is 1.0.4', "pipes/meterian-scan.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"],
    'pipes/sentry-new-release.yml': ['0.3.0 version available. Current version is 0.2.6'],
}


ALLOWED_CATEGORIES = ['Alerting', 'Artifact management', 'Code quality', 'Deployment', 'Feature flagging',
                      'Monitoring', 'Notifications', 'Security', 'Testing', 'Utilities', 'Workflow automation']

REMOTE_METADATA = cerberus.Validator({
    'name': {'type': 'string', 'required': True},
    'image': {'type': 'string', 'required': True},
    'description': {'type': 'string', 'required': True},
    'category': {'type': 'string', 'required': False, 'allowed': ALLOWED_CATEGORIES},
    'repository': {'type': 'string', 'required': True},
    'maintainer': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': False
    },
    'tags': {'type': 'list', 'required': True},

    'vendor': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': False
    },
    'variables': {'type': 'list', 'required': False},
    # TODO: For backward compatibility. Remove `icon` field once custom pipes will migrate
    'icon': {'type': 'string', 'required': False},
})

LOCAL_METADATA = cerberus.Validator({
    'repositoryPath': {'type': 'string', 'required': True},
    'version': {'type': 'string', 'required': True},

    # for backward compatibility with custom pipes. TODO: migrate to proposed structure once custom pipes will migrate
    'name': {'type': 'string', 'required': False},
    'description': {'type': 'string', 'required': False},
    'category': {'type': 'string', 'required': False, 'allowed': ALLOWED_CATEGORIES},
    'logo': {'type': 'string', 'required': False},
    'vendor': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': False
    },
    'maintainer': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': False
    },
    'yml': {'type': 'string', 'required': False},
    'tags': {'type': 'list', 'required': False},
})

FINAL_METADATA = cerberus.Validator({
    'name': {'type': 'string', 'required': True},
    'description': {'type': 'string', 'required': True},
    'category': {'type': 'string', 'required': True, 'allowed': ALLOWED_CATEGORIES},
    'logo': {'type': 'string', 'required': True},
    'repositoryPath': {'type': 'string', 'required': True},
    'version': {'type': 'string', 'required': True},
    'maintainer': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': True
    },
    'vendor': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': False
    },
    'yml': {'type': 'string', 'required': True},
    'tags': {'type': 'list', 'required': True},
    'timestamp': {'type': 'string', 'required': True},
    'created_at': {'type': 'string', 'required': True},
})


class GitApiServiceError(Exception):
    pass


@dataclass
class Failures:
    criticals: List[str]
    warnings: List[str]
    skipped: List[str]


@dataclass
class ProcessMetadata:
    file: str
    valid: bool
    failures: Failures
    errors: List[str]


@dataclass
class ProcessResult:
    metadata: ProcessMetadata
    data: Optional[dict]


@dataclass
class RuleContext:
    infile: str
    local_yml: dict
    remote_yml: Optional[dict]
    final_yml: dict


class DockerHubAPIService:

    def get_size(self, docker_image_path: str) -> int:
        array = docker_image_path.split(':')
        tag = "latest"
        docker_image = array[0]
        if len(array) == 2:
            tag = array[1]
        request = requests.get(f"https://hub.docker.com/v2/repositories/{docker_image}/tags/{tag}")
        request.raise_for_status()
        request.close()
        size = request.json()['full_size']
        return int(size)


class BitbucketCloudService:

    def get_latest_tag(self, repository_path: str) -> Optional[str]:
        """
        Gets the latest tag of a repository (sort alphabetically)
        :param repository_path: Repository path (ex: account/repo)
        :return: Latest tag
        """
        request = requests.get(f"https://api.bitbucket.org/2.0/repositories/{repository_path}/refs/tags?sort=-name&pagelen=100")
        request.raise_for_status()
        tags_objects = request.json()['values']
        request.close()

        version_pattern = re.compile('^(\d+)\.(\d+)\.(\d+)$')
        tags = [tag['name'] for tag in tags_objects if version_pattern.fullmatch(tag['name'])]

        if not tags:
            return None

        def parse_version(version):
            return tuple(map(int, version.split('.')))

        return max(tags, key=parse_version)

    def get_repository_info(self, repository_path: str) -> dict:
        """
        Retrieves repository information
        :param repository_path: Repository path (ex: account/repo)
        :return: JSON response of repository information
        """
        request = requests.get(f"https://api.bitbucket.org/2.0/repositories/{repository_path}/")
        request.raise_for_status()
        request.close()
        return request.json()

    def readme_exists(self, repository_path: str, version: str) -> bool:
        """
        Determine if README.md file exists
        :param repository_path: Repository path (ex: account/repo)
        :param version: Reference (i.e. tag or commit)
        :return: True if exists, False otherwise
        """
        request = requests.head(f"https://bitbucket.org/{repository_path}/raw/{version}/README.md")
        request.raise_for_status()
        request.close()
        return request.status_code == requests.codes.ok

    def get_yml_definition(self, repository_path: str, version: str) -> Optional[str]:
        """
        Determine if README.md file exists
        :param repository_path: Repository path (ex: account/repo)
        :param version: Reference (i.e. tag or commit)
        :return: True if exists, False otherwise
        """
        request = requests.get(f"https://bitbucket.org/{repository_path}/raw/{version}/README.md")
        request.encoding = "utf-8"
        request.close()

        search_group = re.search(r"## YAML Definition(?:(?:.*?\n)*?)```yaml((?:.*?\n)*?)```$", request.text, re.MULTILINE)
        if search_group is None:
            return None
        yml_definition = (search_group.group(1)).lstrip()

        return yml_definition

    def get_pipe_metadata(self, repository_path: str, version: str) -> dict:
        """
        Retrieves pipe metadata
        :param repository_path: Repository path (ex: account/repo)
        :param version: Reference (i.e. tag or commit)
        :return: YAML object
        """
        request = requests.get(f"https://bitbucket.org/{repository_path}/raw/{version}/pipe.yml")
        request.raise_for_status()
        request.close()
        return yaml.safe_load(request.text)


class GitApiService:

    def get_pipe_created_at(self, infile, date_format='iso-strict') -> str:
        '''
        Get pipe's official published date
        param: infile: File path of the pipe's metadata file
        param: date_format: Date format git's supported
        return: Date of the commit of merged pipe's file to master branch
        '''
        # git log --reverse --merges --first-parent master --format=%aD pipes/aws-cloudformation-deploy.yml | head -1
        logs_cmd = f"git log --reverse --merges --first-parent master --date={date_format} --format=%ad -- {infile}"
        head_cmd = "head -1"
        logs = subprocess.Popen(logs_cmd.split(' '),stdout=subprocess.PIPE,)
        result = subprocess.run(head_cmd.split(' '),stdin=logs.stdout, capture_output=True, text=True, encoding='utf-8')
        
        if result.returncode != 0:
            raise GitApiServiceError(result.stderr)

        return result.stdout.strip()

    def get_pipe_updated_at(self, infile: str) -> str:
        raise NotImplementedError()


class Check:

    def __init__(self, context: RuleContext):
        self.failures = Failures(criticals=[], warnings=[], skipped=[])
        self.context = context

    def check(self):
        self.check_git_tag()
        self.check_manifest_file()
        self.check_remote_yml()
        self.check_final_yml()
        self.check_yml_fragment()
        self.check_readme()
        self.check_docker_image_size()
        self.check_pipe_latest_version()
        self.review_skipped()

    def get_failures(self):
        return self.failures

    def check_manifest_file(self):
        if not LOCAL_METADATA.validate(self.context.local_yml):
            self.failures.criticals.append(f"File {self.context.infile} not valid: {str(LOCAL_METADATA.errors)}")

    def check_pipe_latest_version(self):
        yml = self.context.final_yml
        version = yml['version']
        latest_tag = self._get_latest_git_tag()
        if latest_tag != version:
            self.failures.warnings.append(f"{latest_tag} version available. Current version is {version}")

    def check_git_tag(self):
        latest_tag = self._get_latest_git_tag()
        if latest_tag is None:
            self.failures.criticals.append(f"Repository does not contain any tag. Please, release a version first.")

    def _get_latest_git_tag(self):
        bitbucket_api_service = BitbucketCloudService()
        yml = self.context.final_yml
        repository_path = yml['repositoryPath']
        # Get latest tag
        return bitbucket_api_service.get_latest_tag(repository_path)

    def check_readme(self):
        bitbucket_api_service = BitbucketCloudService()
        yml = self.context.final_yml
        version = yml['version']
        repository_path = yml['repositoryPath']
        # Fetch the README.md
        if not bitbucket_api_service.readme_exists(repository_path, version):
            self.failures.criticals.append(f"{self.context.infile} repository not valid: README.md not found")

    def check_yml_fragment(self):
        bitbucket_api_service = BitbucketCloudService()
        yml = self.context.final_yml
        infile = self.context.infile
        version = yml['version']
        repository_path = yml['repositoryPath']
        yml_definition = bitbucket_api_service.get_yml_definition(repository_path, version)
        if 'yml' in yml and yml_definition != yml['yml']:
            # TODO: check this until we make sure all code snippets are similar
            self.failures.criticals.append(f"{infile} yml field not equal")
        if not yml['yml']:
            self.failures.criticals.append(f"{infile} yml definition in README.md does not exist")
        try:
            yaml.safe_load(yml['yml'])
        except yaml.YAMLError as exc:
            self.failures.criticals.append(f"{infile} yml definition in README.md is not valid")

    def check_yml_definition_in_readme(self):
        infile = self.context.infile
        yml = self.context.final_yml
        # validate YAML file
        try:
            yaml.safe_load(yml['yml'])
        except yaml.YAMLError as exc:
            self.failures.criticals.append(f"{infile} yml definition in README.md is not valid")

    def check_docker_image_size(self):
        if not self.context.remote_yml:
            return

        dockerhub_api_service = DockerHubAPIService()
        image_name = self.context.remote_yml['image']
        # Validate docker image size
        try:
            image_size = dockerhub_api_service.get_size(image_name)
        except:
            client = docker.from_env()
            image = client.images.pull(image_name)
            client.close()
            image_size = int(image.attrs['Size'])

        if image_size > 1073741824:
            self.failures.criticals.append(f"Docker image '{image_name}' is larger than 1GB.")

    def check_remote_yml(self):
        infile = self.context.infile
        if not REMOTE_METADATA.validate(self.context.remote_yml):
            # TODO: remove when all pipes migrate to new metadata
            if self.context.remote_yml.get('icon') or not isinstance(self.context.remote_yml['maintainer'], dict):
                # This means the vendor pipe is not migrated yet to new metadata structure
                self.failures.warnings.append(
                    f"{infile} repository not valid: pipe.yml not valid: {str(REMOTE_METADATA.errors)}. "
                    f"Notify maintainer")
                return

            self.failures.criticals.append(
                f"{infile} repository not valid: pipe.yml not valid: {str(REMOTE_METADATA.errors)}")

    def check_final_yml(self):
        infile = self.context.infile
        if not FINAL_METADATA.validate(self.context.final_yml):
            # TODO: remove when all pipes migrate to new metadata
            if self.context.final_yml.get('icon') or not isinstance(self.context.final_yml['maintainer'], dict):
                # This means the vendor pipe is not migrated yet to new metadata structure
                self.failures.warnings.append(f"{infile} final metadata not valid: {str(FINAL_METADATA.errors)}. "
                                              f"Notify maintainer.")
                return

            self.failures.criticals.append(f"{infile} final metadata not valid: {str(FINAL_METADATA.errors)}")

    def review_skipped(self):
        self.generate_skipped()
        self.failures.criticals = [item for item in self.failures.criticals if item not in self.failures.skipped]
        self.failures.warnings = [item for item in self.failures.warnings if item not in self.failures.skipped]

    def generate_skipped(self):
        self.failures.skipped = []
        if self.context.infile in WHITELIST_PIPES:
            for critical in self.failures.criticals:
                if critical in WHITELIST_PIPES[self.context.infile]:
                    self.failures.skipped.append(critical)

            for warning in self.failures.warnings:
                if warning in WHITELIST_PIPES[self.context.infile]:
                    self.failures.skipped.append(warning)


def validate(context: RuleContext) -> tuple:
    infile = context.infile
    # RuleEngine
    check = Check(context)
    check.check()

    return check.get_failures()


def extract_information(local_yml: dict, infile: str) -> RuleContext:
    bitbucket_api_service = BitbucketCloudService()

    local_yml_valid = LOCAL_METADATA.validate(local_yml)
    if not local_yml_valid:
        warnings.warn("Structure you are using, is deprecated. Please, update to current one", DeprecationWarning)

    final_yml = local_yml.copy()
    remote_yml = None

    repository_path = final_yml['repositoryPath']
    version = final_yml['version']

    # Fetch Repository information
    if 'logo' not in final_yml:
        try:
            repo_info = bitbucket_api_service.get_repository_info(repository_path)
            final_yml['logo'] = repo_info['links']['avatar']['href']
        except HTTPError as error:
            logger.debug(error)
    if 'yml' not in final_yml:
        try:
            yml_definition = bitbucket_api_service.get_yml_definition(repository_path, version)
            final_yml['yml'] = yml_definition
        except HTTPError as error:
            logger.debug(error)
    # Fetch the pipe.yml (metadata)
    try:
        remote_yml = bitbucket_api_service.get_pipe_metadata(repository_path, version)
        remote_yml_valid = REMOTE_METADATA.validate(remote_yml)
        if not remote_yml_valid:
            warnings.warn("Structure you are using, is deprecated. Please, update to current one", DeprecationWarning)
    except HTTPError as error:
        logger.debug(error)

    # TODO: remove once custom pipes will change to new version
    if 'maintainer' in final_yml:
        remote_yml.pop('maintainer', None)
    if 'vendor' in final_yml:
        remote_yml.pop('vendor', None)

    final_yml = {key: value for key, value in {**final_yml, **remote_yml}.items() if key in FINAL_METADATA.schema}

    git_api_service = GitApiService()
    created_at = CREATED_AT_PIPES.get(infile)
    final_yml['created_at'] = created_at if created_at else git_api_service.get_pipe_created_at(infile, 'iso-strict')
    # timestamp format rfc2822
    timestamp = utils.format_datetime(datetime.fromisoformat(final_yml['created_at']))
    # deprecated move all logic to created_at
    final_yml['timestamp'] = f"\"b'{timestamp}\\n'\""

    # TODO add final_yml['updated_at'] as datetime of the current verision

    return RuleContext(infile=infile, local_yml=local_yml, remote_yml=remote_yml, final_yml=final_yml)


def process(infile: str) -> ProcessResult:
    failures = Failures(criticals=[], warnings=[], skipped = [])
    errors = []
    yaml_file = None

    with open(infile, 'r') as stream:
        try:
            manifest = yaml.safe_load(stream)
            context = extract_information(local_yml=manifest, infile=infile)
            yaml_file = context.final_yml
            failures = validate(context)
        except Exception as e:
            errors.append(str(e))

        print(
            f"Pipe: {infile}. "
            f"Criticals: {len(failures.criticals)} "
            f"Warnings: {len(failures.warnings)} "
            f"Errors: {len(errors)} "
            f"Skipped: {len(failures.skipped)}"
        )
        return ProcessResult(
            metadata=ProcessMetadata(
                file=infile,
                valid=(len(failures.criticals) == 0 and len(failures.warnings) == 0 and len(errors) == 0),
                failures=failures,
                errors=errors),
            data=yaml_file)


def extract_data(results: List[ProcessResult]) -> List[dict]:
    data: List[dict] = [result.data for result in results if result.data]
    return data


def extract_errors(results: List[ProcessResult]) -> List[ProcessResult]:
    errors: List[ProcessResult] = [x for x in results if not x.metadata.valid]
    return errors


def main():
    path = sys.argv[1] if len(sys.argv) > 1 else 'pipes/*.yml'
    list_files = sorted(glob.glob(path))
    logger.info("Starting: " + str(len(list_files)) + " pipe(s) to verify.")

    t1 = datetime.now()

    with ThreadPoolExecutor() as executor:
        results = list(executor.map(process, list_files))

        errors = extract_errors(results)
        atlassian_maintainer_pipe_failed = False
        if errors:
            for error in errors:
                if error.data is None:
                    logger.error(f"{asdict(error.metadata)}")
                    continue

                maintainer = error.data.get('maintainer')
                # TODO: remove when all pipes migrate to new metadata
                maintainer_name = maintainer['name'] if isinstance(maintainer, dict) else maintainer
                if maintainer_name == 'Atlassian':
                    atlassian_maintainer_pipe_failed = True
                    logger.error(f"{asdict(error.metadata)}")
                elif error.metadata.failures.criticals:
                    logger.error(f"{asdict(error.metadata)}")
                else:
                    logger.warning(f"{asdict(error.metadata)}")
            # fail build for any Atlassian's pipes error or any critical failures
            if (any(err for err in errors if atlassian_maintainer_pipe_failed) or error.metadata.failures.criticals):
                logger.error("Found critical errors. Build has failed.")
                sys.exit(1)

        data = extract_data(results)

    with open('pipes.json', 'w', encoding='utf8') as outfile:
        json.dump(data, outfile, ensure_ascii=False)

    t2 = datetime.now()
    logger.info("Finished. Time: " + str(t2 - t1))


if __name__ == '__main__':
    main()
