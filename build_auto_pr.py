import os
import re
import glob
import time

import git
import yaml
import colorlog
import requests
from requests.auth import HTTPBasicAuth


# TODO Schedule IT: each 1h

logger = colorlog.getLogger()

handler = colorlog.StreamHandler()
handler.setFormatter(colorlog.ColoredFormatter(
    '%(log_color)s%(levelname)s: %(message)s'))
logger.addHandler(handler)
logger.setLevel('INFO')
colorlog.default_log_colors['INFO'] = 'white'


PIPES_LOCAL_PATH = 'pipes/*.yml'
BITBUCKET_API_BASE_URL = 'https://api.bitbucket.org/2.0'
BITBUCKET_REPOSITORY_URL = f"{BITBUCKET_API_BASE_URL}/repositories/bitbucketpipelines/official-pipes"
BITBUCKET_AUTO_PULL_REQUEST_TITLE = "Auto Bump version for pipes"


class RepositoryHasNoTagsError(Exception):
    pass


class CreatePRBitbucketError(Exception):
    pass


class GetPRsBitbucketError(Exception):
    pass


class PipeVersionManager:
    def __init__(self, local_path):
        self.pipes_files = []
        self.pipes_metadata = {}
        self.pipes_files_to_update = []
        # TODO implement a notifier
        self.pipes_files_to_notify = []
        self.setup(local_path)

    def setup(self, local_path):
        self.set_pipes_list(local_path)
        logger.info(f"There are {len(self.pipes_files)} official-pipes:\n"
                    f"{self.pipes_files}\n")

        logger.info('Starting to get metadata:')
        for pipe_file in self.pipes_files:
            self.set_local_pipe_metadata(pipe_file)
            logger.info(f"metadata {pipe_file} complete.")
            self.set_remote_version(pipe_file)
            logger.info(f"remote version for {pipe_file} complete.")
            # pipes to update
            # auto update only internal pipes
            if self.is_pipe_internal(pipe_file):
                self.set_pipe_to_update(pipe_file)
            else:
                # pipes to notify
                # notify external pipes maintainers
                self.set_pipe_to_notify(pipe_file)

        if self.pipes_files_to_update:
            logger.warning(f"Bump version. Number of files changed: "
                           f"{len(self.pipes_files_to_update)}:")
            for pipe in self.pipes_files_to_update:
                logger.warning(f"{pipe}")

    def get_repository_path(self, pipe_file):
        return self.pipes_metadata[pipe_file]['repository_path']

    def get_local_version(self, pipe_file):
        return self.pipes_metadata[pipe_file]['version']['local']

    def get_remote_version(self, pipe_file):
        return self.pipes_metadata[pipe_file]['version']['remote']

    def get_internal_pipe_files(self):
        return [pipe_file for pipe_file in self.pipes_files if self.is_pipe_internal(pipe_file)]

    def get_external_pipe_files(self):
        return [pipe_file for pipe_file in self.pipes_files if not self.is_pipe_internal(pipe_file)]

    def is_pipe_internal(self, pipe_file):
        maintainer = self.pipes_metadata[pipe_file].get('maintainer')
        if not maintainer:
            pipe = self.get_pipe_data(pipe_file)
            maintainer = pipe['maintainer']

        # TODO: make to migrate all pipes
        if isinstance(maintainer, dict) and 'name' in maintainer:
            maintainer = maintainer['name']

        return maintainer == 'Atlassian'

    def get_pipe_data(self, pipe_file):
        repository_path = self.get_repository_path(pipe_file)
        version = self.get_remote_version(pipe_file)
        with requests.get(f"https://bitbucket.org/{repository_path}/raw/{version}/pipe.yml") as response:
            response.raise_for_status()
            pipe = yaml.safe_load(response.text)
        return pipe

    def set_pipes_list(self, local_path):
        self.pipes_files = sorted(glob.glob(local_path))

    def set_local_pipe_metadata(self, pipe_file):
        with open(pipe_file, 'r') as stream:
            yaml_file = yaml.safe_load(stream)
            self.pipes_metadata[pipe_file] = {
                'file': pipe_file,
                'repository_path': yaml_file.get('repositoryPath'),
                'version': {
                    'remote': None,
                    'local': yaml_file.get('version'),
                },
                'maintainer': yaml_file.get('maintainer')
            }

    def set_remote_version(self, pipe_file):
        repository_path = self.get_repository_path(pipe_file)
        self.pipes_metadata[pipe_file]['version']['remote'] = \
            self._get_latest_tag(repository_path)

    def _get_latest_tag(self, repository_path):
        """
        Gets the latest tag of a repository
        :param repository_path: Repository path (ex: account/repo)
        :return: Latest tag
        """
        REPOSITORY_TAGS_URL = f"https://api.bitbucket.org/2.0/repositories/{repository_path}/refs/tags?sort=-name&pagelen=100"
        request_tags = requests.get(REPOSITORY_TAGS_URL)
        tags_objects = request_tags.json()['values']
        # get only tags with format 0.0.0
        pattern = re.compile("^([0-9]+)\.([0-9]+)\.([0-9]+)$")
        tags = [tag['name'] for tag in tags_objects if pattern.match(tag['name'])]

        if not tags:
            raise RepositoryHasNoTagsError(repository_path)

        return max(tags, key=parse_version)

    def set_pipe_to_update(self, pipe_file):
        local_version = parse_version(self.get_local_version(pipe_file))
        remote_version = parse_version(self.get_remote_version(pipe_file))

        if local_version < remote_version:
            self.pipes_files_to_update.append(pipe_file)

    def set_pipe_to_notify(self, pipe_file):
        local_version = parse_version(self.get_local_version(pipe_file))
        remote_version = parse_version(self.get_remote_version(pipe_file))

        if local_version < remote_version:
            self.pipes_files_to_notify.append(pipe_file)

    def update_all_versions(self):
        # update local files
        for pipe_file in self.pipes_files_to_update:
            self.update_version(pipe_file)

    def update_version(self, pipe_file):
        self._update_version_in_file(
            pipe_file,
            self.get_local_version(pipe_file),
            self.get_remote_version(pipe_file)
        )

    @staticmethod
    def _update_version_in_file(pipe_file, local_version, remote_version):
        with open(pipe_file, 'r') as f:
            pipe = f.read()
        pipe = pipe.replace(f"version: '{local_version}'", f"version: '{remote_version}'")
        with open(pipe_file, 'w') as f:
            f.write(pipe)

    def make_pull_request(self):
        logger.info('Preparing for pull request.')

        username = os.getenv('BITBUCKET_USERNAME')
        app_password = os.getenv('BITBUCKET_APP_PASSWORD')

        current_repo = git.Repo('.', odbt=git.GitDB)
        origin = current_repo.remotes.origin.set_url(f'https://{username}:{app_password}@bitbucket.org/bitbucketpipelines/official-pipes.git')
        current_branch = current_repo.active_branch.name

        branch_name = f"auto-bump-pipes-versions-{get_uniq_timestamp()}"
        new_version_branch = current_repo.head.ref.checkout(b=branch_name)

        logger.info(f"Created and checkouted to the new branch {new_version_branch}.")
        new_version_branch.repo.index.add(self.pipes_files_to_update)
        new_version_branch.repo.index.commit(
            f"Auto Bump version for {len(self.pipes_files_to_update)} pipes")

        logger.info("Commited changes.")

        # push new branch to the remote
        origin = new_version_branch.repo.remotes[0]
        origin.push(new_version_branch)

        logger.info(f"Pushed the branch {new_version_branch} to the remote "
                    f"https://bitbucket.org/bitbucketpipelines/official-pipes/branch/{new_version_branch} .")

        response = self._make_pull_request(branch_name)
        pr_link = response.json()['links']['html']['href']

        logger.info(f'Successfully PR created {pr_link}.')

    def _make_pull_request(self, source_branch, destination_branch="master"):
        # create PR with Bitbucket API
        bitbucket_pr_url = f"{BITBUCKET_REPOSITORY_URL}/pullrequests"
        bitbucket_reviewers_url = f"{BITBUCKET_REPOSITORY_URL}/default-reviewers"

        username = os.getenv('BITBUCKET_USERNAME')
        app_password = os.getenv('BITBUCKET_APP_PASSWORD')

        auth = HTTPBasicAuth(username, app_password)

        headers = {'Content-Type': 'application/json'}

        # default reviewers
        response = requests.get(bitbucket_reviewers_url, headers=headers, auth=auth)
        # TODO remove "if user["uuid"]" after using bot account app_password
        default_reviewers = [{"uuid": user["uuid"]} for user in response.json()['values']]

        data = {
            "title": BITBUCKET_AUTO_PULL_REQUEST_TITLE,
            "source": {
                "branch": {
                    "name": source_branch
                }
            },
            "destination": {
                "branch": {
                    "name": destination_branch
                }
            },
            "description": f"Bump version. Number of files changed: {len(self.pipes_files_to_update)}",
            "reviewers": default_reviewers
        }

        response = requests.post(bitbucket_pr_url, headers=headers, auth=auth, json=data)
        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            raise CreatePRBitbucketError(response.text)

        return response

    def is_pull_request_open(self, pull_request_title):
        # TODO DRY make separate class BitbucketRepositoryAPI
        # create PR with Bitbucket API
        bitbucket_pr_url = f"{BITBUCKET_REPOSITORY_URL}/pullrequests"

        username = os.getenv('BITBUCKET_USERNAME')
        app_password = os.getenv('BITBUCKET_APP_PASSWORD')

        auth = HTTPBasicAuth(username, app_password)

        headers = {'Content-Type': 'application/json'}

        response = requests.get(f"{bitbucket_pr_url}?status=open", headers=headers, auth=auth)
        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError:
            raise GetPRsBitbucketError(response.text)

        return any(pr['title'] == pull_request_title for pr in response.json()['values'])


def get_uniq_timestamp():
    return str(time.time()).split('.')[0]


def parse_version(version):
    return tuple(map(int, version.split('.')))


def main():
    manager = PipeVersionManager(PIPES_LOCAL_PATH)
    # update internal pipes
    is_auto_pull_request_open = manager.is_pull_request_open(BITBUCKET_AUTO_PULL_REQUEST_TITLE)
    if manager.pipes_files_to_update and not is_auto_pull_request_open:
        manager.update_all_versions()
        manager.make_pull_request()
    elif is_auto_pull_request_open:
        logger.info(
            f"Pull-request exists and is waiting for merge.\n"
            f"https://bitbucket.org/bitbucketpipelines/official-pipes/pull-requests"
        )
    else:
        logger.info("Nothing to update. All internal pipe's versions are the latest.")

    # external pipes
    if manager.pipes_files_to_notify:
        logger.warning(f"Notify external pipe's developers to keep pipe's versions in the Official pipes repository up to date.")
        for pipe_file in manager.pipes_files_to_notify:
            logger.warning(pipe_file)


if __name__ == '__main__':
    main()
